package com.automationanywhere.botcommand.google.automl;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class DrawBoxCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(DrawBoxCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    DrawBox command = new DrawBox();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("input") && parameters.get("input") != null && parameters.get("input").get() != null) {
      convertedParameters.put("input", parameters.get("input").get());
      if(convertedParameters.get("input") !=null && !(convertedParameters.get("input") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","input", "String", parameters.get("input").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("input") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","input"));
    }

    if(parameters.containsKey("output") && parameters.get("output") != null && parameters.get("output").get() != null) {
      convertedParameters.put("output", parameters.get("output").get());
      if(convertedParameters.get("output") !=null && !(convertedParameters.get("output") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","output", "String", parameters.get("output").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("output") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","output"));
    }

    if(parameters.containsKey("suboutput") && parameters.get("suboutput") != null && parameters.get("suboutput").get() != null) {
      convertedParameters.put("suboutput", parameters.get("suboutput").get());
      if(convertedParameters.get("suboutput") !=null && !(convertedParameters.get("suboutput") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","suboutput", "String", parameters.get("suboutput").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("suboutput") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","suboutput"));
    }

    if(parameters.containsKey("annotation") && parameters.get("annotation") != null && parameters.get("annotation").get() != null) {
      convertedParameters.put("annotation", parameters.get("annotation").get());
      if(convertedParameters.get("annotation") !=null && !(convertedParameters.get("annotation") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","annotation", "String", parameters.get("annotation").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("jsonBox") && parameters.get("jsonBox") != null && parameters.get("jsonBox").get() != null) {
      convertedParameters.put("jsonBox", parameters.get("jsonBox").get());
      if(convertedParameters.get("jsonBox") !=null && !(convertedParameters.get("jsonBox") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","jsonBox", "String", parameters.get("jsonBox").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("jsonBox") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","jsonBox"));
    }

    if(parameters.containsKey("normalized") && parameters.get("normalized") != null && parameters.get("normalized").get() != null) {
      convertedParameters.put("normalized", parameters.get("normalized").get());
      if(convertedParameters.get("normalized") !=null && !(convertedParameters.get("normalized") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","normalized", "String", parameters.get("normalized").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("normalized") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","normalized"));
    }
    if(convertedParameters.get("normalized") != null) {
      switch((String)convertedParameters.get("normalized")) {
        case "yes" : {

        } break;
        case "no" : {

        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","normalized"));
      }
    }

    try {
      command.action((String)convertedParameters.get("input"),(String)convertedParameters.get("output"),(String)convertedParameters.get("suboutput"),(String)convertedParameters.get("annotation"),(String)convertedParameters.get("jsonBox"),(String)convertedParameters.get("normalized"));Optional<Value> result = Optional.empty();
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
