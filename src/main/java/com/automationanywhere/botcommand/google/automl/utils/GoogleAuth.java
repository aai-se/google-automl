package com.automationanywhere.botcommand.google.automl.utils;



import com.google.api.client.json.JsonFactory;
import com.google.auth.oauth2.ServiceAccountCredentials;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;


import java.net.URLEncoder;


public class GoogleAuth {
	



		
	    public static final String AUTH_URI = "https://accounts.google.com/o/oauth2/auth";
	    public static final String TOKEN_URI = "https://oauth2.googleapis.com/token";
	    public static final String AUTH_PROVIDER=  "https://www.googleapis.com/oauth2/v1/certs";
	    public static final String CERTURLBASE =  "https://www.googleapis.com/robot/v1/metadata/x509/";
	 
	    

	    private static final Logger LOGGER = LogManager.getLogger(GoogleAuth.class);



	    private ServiceAccountCredentials credential;

	   public String writeAutFile(String projectID, String private_key_id, String private_key, String client_email, String client_id) throws Exception {
		   
	       String property = "java.io.tmpdir";
	        String tempDir = System.getProperty("java.io.tmpdir");
	        File authfile = new File(tempDir + "/googleauth.json");
		    if (authfile.exists()) {
		    	authfile.delete();
		    } 
		    
		       
  	         String encodedPrivateKey = "-----BEGIN PRIVATE KEY-----\n"+private_key.replaceAll("\n", "").replaceAll("\r", "").replaceAll("-----BEGIN PRIVATE KEY-----","").replaceAll("-----END PRIVATE KEY-----","")+"\n-----END PRIVATE KEY-----";
            System.out.println("KEY    "+encodedPrivateKey);
		    
		    JSONObject authData= new JSONObject();
		    
		    authData.put("type", "service_account");
		    authData.put("project_id", projectID);
		    authData.put("private_key_id",  private_key_id);
		    authData.put("private_key", encodedPrivateKey);
		    authData.put("client_email", client_email);
		    authData.put("client_id", client_id);
		    authData.put("auth_uri", AUTH_URI);
		    authData.put("token_uri", TOKEN_URI);
		    authData.put("auth_provider_x509_cert_url", AUTH_PROVIDER);
		    authData.put("client_x509_cert_url", CERTURLBASE+URLEncoder.encode(client_email, "UTF-8"));
		    
		    FileWriter writer = new FileWriter(authfile);
		    writer.write(authData.toString());
		    writer.close();
		    
		    return authfile.getAbsolutePath();
		   
	   }
	    
	    public ServiceAccountCredentials getGoogleCredential(String projectID, String private_key_id, String private_key, String client_email, String client_id) throws Exception {

	        LOGGER.info("Started fetching details..... in GOOGLE CREDENTIALS");

	        if (getCredential() != null) {
	            System.out.println("Returned already...");
	            return credential;
	        }
	        

	        String authFile = writeAutFile(projectID, private_key_id, private_key, client_email, client_id);
			credential = ServiceAccountCredentials.fromStream(new FileInputStream(new File(authFile)));
	    
	        return credential;
	    }


	    public ServiceAccountCredentials getCredential() {
	        return credential;
	    }
}



