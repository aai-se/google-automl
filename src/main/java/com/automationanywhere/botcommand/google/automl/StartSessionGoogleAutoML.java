/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */

package com.automationanywhere.botcommand.google.automl;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Map;

import com.automationanywhere.bot.service.GlobalSessionContext;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.google.automl.utils.GoogleAutoML;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;




/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Start Session", name = "StartGoogleAutoML", description = "Start new session", 
 icon = "pkg.svg", background_color = "#EDEDED" , comment = true ,node_label = "Start Session {{sessionName}}|") 

public class StartSessionGoogleAutoML{
 
    @Sessions
    private Map<String, Object> sessions;
    
	  
	@com.automationanywhere.commandsdk.annotations.GlobalSessionContext
	private GlobalSessionContext globalSessionContext;

	  
	  public void setGlobalSessionContext(GlobalSessionContext globalSessionContext) {
	        this.globalSessionContext = globalSessionContext;
	    }
	  
	  
   private 	GoogleAutoML autoML ;
	
    
    @Execute
    public void start(@Idx(index = "1", type = TEXT) @Pkg(label = "Session name",  default_value_type = STRING, default_value = "AutoMLSession") @NotEmpty String sessionName,
	          		  @Idx(index = "2", type = AttributeType.FILE)  @Pkg(label = "Google API Authentication File (JSON)" , default_value_type = DataType.FILE) @NotEmpty  String jsonPath,
	          		  @Idx(index = "3", type = TEXT) @Pkg(label = "Project ID",  default_value_type = STRING) @NotEmpty String projectID,
	          		  @Idx(index = "4", type = AttributeType.SELECT, options = {
						@Idx.Option(index = "4.1", pkg = @Pkg(label = "US Central1", value = "us-central1"))}) 
    				    @Pkg(label = "Location", default_value = "us-central1", default_value_type = STRING) @NotEmpty String location ) throws Exception {

 
        // Check for existing session
        if (sessions.containsKey(sessionName))
            throw new BotCommandException("Session name in use ") ;
        
    	this.autoML = new GoogleAutoML(projectID,location);
    	
    	autoML.auth(jsonPath);

        this.sessions.put(sessionName, this.autoML);


 
    }
 
    
    
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
    

    
    
 
    
    
}