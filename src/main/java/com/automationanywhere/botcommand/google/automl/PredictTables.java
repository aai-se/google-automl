/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */


package com.automationanywhere.botcommand.google.automl;

import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.google.automl.utils.GoogleAutoML;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.commandsdk.annotations.Execute;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Predict Table", name = "automltablespredict",
        description = "Predictions for Table Values",
        node_label = "Predict Table {{sessionName}}|", icon = "pkg.svg", background_color = "#EDEDED" , comment = true ,
        return_type=DataType.DICTIONARY, return_sub_type = DataType.ANY,  return_label="Predictions", return_required=true)

public class PredictTables {
	
	  private static final Logger logger = LogManager.getLogger(PredictTables.class);
	
    @Sessions
    private Map<String, Object> sessions;

	
	   
	@Execute
    public DictionaryValue  action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "AutoMLSession") @NotEmpty String sessionName,
    						       @Idx(index = "2", type = TEXT)  @Pkg(label = "Model ID" , default_value_type = STRING, return_sub_type = STRING ) @NotEmpty String modelId,
    						       @Idx(index = "3", type = AttributeType.DICTIONARY)  @Pkg(label = "Values" , default_value_type = DataType.DICTIONARY, return_sub_type = STRING, description = "Dictionary of <Column Name,Value>") @NotEmpty Map<String,Value> values,
    						       @Idx(index = "4", type = AttributeType.DICTIONARY)  @Pkg(label = "Columns" , default_value_type = DataType.DICTIONARY, return_sub_type = STRING,  description = "Dictionary of <Column Name,Spec ID>") @NotEmpty Map<String,Value> columns
    						 ) throws Exception { 
		


	    GoogleAutoML autoML  = (GoogleAutoML) this.sessions.get(sessionName);  
	    
	    HashMap<String, String> valueMap = new HashMap<String,String>();
	    HashMap<String, String> columnMap = new HashMap<String,String>();

		for (Entry<String, Value> value:values.entrySet()) {
			valueMap.put(value.getKey(),value.getValue().toString());
			
		}
		
		for (Entry<String, Value> column:columns.entrySet()) {
			columnMap.put(column.getKey(),column.getValue().toString());
		}


		HashMap<String, Object> result = autoML.predictTables(modelId,  valueMap, columnMap);
	    
	    HashMap<String,Value> resultMap = new HashMap<String,Value>();
	    for (Entry<String, Object> entry : result.entrySet()) {
	    	String classname = entry.getValue().getClass().getSimpleName();

			switch (classname) {
				case "String":
					resultMap.put(entry.getKey(), new StringValue((String)entry.getValue()));
					break;
				case "Integer":
					resultMap.put(entry.getKey(), new NumberValue((Integer)entry.getValue()));
					break;
				case "Long":
					resultMap.put(entry.getKey(), new NumberValue((Long)entry.getValue()));
					break;
				case "Double":
					resultMap.put(entry.getKey(), new NumberValue((Double)entry.getValue()));
					break;
				case "Float":
					resultMap.put(entry.getKey(), new NumberValue((Float)entry.getValue()));
					break;
				case "Boolean":
					resultMap.put(entry.getKey(), new BooleanValue((Boolean)entry.getValue()));
					break;
				default:
					resultMap.put(entry.getKey(), new StringValue(entry.getValue().toString()));
					break;
			}
			
		}
	    
	    DictionaryValue resultDict = new DictionaryValue();
	    resultDict.set(resultMap);
	    
	    return resultDict;

	
	}

	
	public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
   	
	
}