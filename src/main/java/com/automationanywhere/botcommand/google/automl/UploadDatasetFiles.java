/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */


package com.automationanywhere.botcommand.google.automl;

import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.google.automl.utils.GoogleAutoML;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.commandsdk.annotations.Execute;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Upload Dataset Files", name = "uploadDatasetFiles",
        description = "Upload files to create a dataset",
        node_label = "Upload Dataset Files {{sessionName}}|", icon = "pkg.svg", background_color = "#EDEDED" , comment = true ,
        return_type=DataType.STRING,  return_label="Google Bucket URI", return_required=false)

public class UploadDatasetFiles {
	
	  private static final Logger logger = LogManager.getLogger(UploadDatasetFiles.class);
	
    @Sessions
    private Map<String, Object> sessions;
    
	   
	@Execute
    public StringValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "AutoMLSession") @NotEmpty String sessionName,
    							  @Idx(index = "2", type = TEXT)  @Pkg(label = "Bucket Name" , default_value_type = STRING ) @NotEmpty String bucketName,
    							  @Idx(index = "3", type = TEXT)  @Pkg(label = "Parent Bucket Folder" , default_value_type = STRING ) String folderPath,
    							  @Idx(index = "4", type = TEXT)  @Pkg(label = "Local File directory" , default_value_type = STRING ) @NotEmpty String dirPath,
    							  @Idx(index = "5", type = SELECT, options = {
    	            					    @Idx.Option(index ="5.1", pkg = @Pkg(label = "TEXTEXTRACTION", value = GoogleAutoML.TEXTEXTRACTION)),
    	            					    @Idx.Option(index ="5.2", pkg = @Pkg(label = "VISIONCLASSIFY", value = GoogleAutoML.VISIONCLASSIFY)),
    	            					    @Idx.Option(index ="5.3", pkg = @Pkg(label = "VISIONDETECTION", value = GoogleAutoML.VISIONDETECTION))	
    							  })  @Pkg(label = "AutoML Type"  , default_value = "TEXTEXTRACTION", default_value_type = DataType.STRING , return_sub_type = DataType.STRING) @NotEmpty String autoMLtype
    							  ) throws Exception { 


	    GoogleAutoML autoML  = (GoogleAutoML) this.sessions.get(sessionName);  
	    
	    folderPath = (folderPath == null) ? "" : folderPath;
	    	    
	    String gcsURI = autoML.uploadDatasetFiles( bucketName, "" , folderPath, dirPath, autoMLtype);
	    
	    return new StringValue(gcsURI);

	
	}

	
	public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
   	
	
}