/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */


package com.automationanywhere.botcommand.google.automl;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.google.automl.utils.GoogleAutoML;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.google.cloud.automl.v1.ClassificationType;
import com.automationanywhere.commandsdk.annotations.Execute;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Import Files to Dataset", name = "importDataset",
        description = "Import GCS files to dataset",
        node_label = "Import Files to Dataset {{sessionName}}|", icon = "pkg.svg", background_color = "#EDEDED" , comment = true ,
        return_type=DataType.STRING,  return_label="Operation", return_required=false)

public class ImportDataset {
	
	  private static final Logger logger = LogManager.getLogger(ImportDataset.class);
	
    @Sessions
    private Map<String, Object> sessions;
    
	
	   
	@Execute
    public StringValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "AutoMLSession") @NotEmpty String sessionName,
    					      @Idx(index = "2", type = TEXT)  @Pkg(label = "Dataset ID" , default_value_type = STRING ) @NotEmpty String datasetId,
    					      @Idx(index = "3", type = TEXT)  @Pkg(label = "GCS Path (URI)" , default_value_type = STRING ) @NotEmpty String path
    					     ) throws Exception { 


	    GoogleAutoML autoML  = (GoogleAutoML) this.sessions.get(sessionName);  
	    
	    String operation = autoML.importDataset(datasetId,  path);

	    return new StringValue(operation);

	
	}

	
	public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
   	
	
}