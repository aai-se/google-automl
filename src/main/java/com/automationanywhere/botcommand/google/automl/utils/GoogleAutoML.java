package com.automationanywhere.botcommand.google.automl.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.api.gax.core.CredentialsProvider;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.api.gax.longrunning.OperationFuture;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.api.gax.longrunning.OperationFuture;
import com.google.cloud.automl.v1.AnnotationPayload;
import com.google.cloud.automl.v1.AutoMlClient;
import com.google.cloud.automl.v1.ClassificationType;
import com.google.cloud.automl.v1.Dataset;
import com.google.cloud.automl.v1.DatasetName;
import com.google.cloud.automl.v1.ExamplePayload;
import com.google.cloud.automl.v1.GcsSource;
import com.google.cloud.automl.v1.Image;
import com.google.cloud.automl.v1.ImageClassificationDatasetMetadata;
import com.google.cloud.automl.v1.ImageObjectDetectionDatasetMetadata;
import com.google.cloud.automl.v1.InputConfig;
import com.google.cloud.automl.v1.ListModelsRequest;
import com.google.cloud.automl.v1.LocationName;
import com.google.cloud.automl.v1.Model;
import com.google.cloud.automl.v1.ModelName;
import com.google.cloud.automl.v1.NormalizedVertex;
import com.google.cloud.automl.v1.OperationMetadata;
import com.google.cloud.automl.v1.PredictRequest;
import com.google.cloud.automl.v1.PredictResponse;
import com.google.cloud.automl.v1.PredictionServiceClient;
import com.google.cloud.automl.v1.PredictionServiceSettings;
import com.google.cloud.automl.v1.TextClassificationDatasetMetadata;
import com.google.cloud.automl.v1.TextExtractionDatasetMetadata;
import com.google.cloud.automl.v1.TextExtractionModelMetadata;
import com.google.cloud.automl.v1.TextSnippet;
import com.google.cloud.automl.v1beta1.Row;
import com.google.cloud.automl.v1beta1.Row.Builder;
import com.google.cloud.automl.v1beta1.TablesDatasetMetadata;
import com.google.cloud.automl.v1beta1.TablesModelColumnInfo;
import com.google.cloud.automl.v1beta1.TablesModelMetadata;
import com.google.cloud.automl.v1beta1.TextSegment;
import com.google.protobuf.ByteString;
import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.Empty;
import com.google.protobuf.Value;
import com.google.cloud.automl.v1.AutoMlSettings;
import com.google.cloud.automl.v1.BoundingPoly;

public class GoogleAutoML {
	
	public static final String VISIONCLASSIFY = "VC";
	public static final String VISIONDETECTION = "VD";
	public static final String TEXTEXTRACTION = "TE";
	public static final String TEXTCLASSIFY = "TC";
	public static final String TABLESPREDICT = "TP";
	
	private GoogleStorageBucket storage;
	private AutoMlClient datasetServiceClient;
	private PredictionServiceClient predictionclient;
	private com.google.cloud.automl.v1beta1.AutoMlClient tablesClient;
	private com.google.cloud.automl.v1beta1.PredictionServiceClient predictiontableclient;
	
	private String location;
	private String projectID;
	
	public GoogleAutoML(String project, String loc) {
		this.projectID = project;
		this.location = loc;
	}
	
	public void auth(String jsonPath) throws Exception {
	     storage = new GoogleStorageBucket();
	     storage.auth(jsonPath);
			  
		 CredentialsProvider credentialsProvider = FixedCredentialsProvider.create(ServiceAccountCredentials.fromStream(new FileInputStream(jsonPath)));
		 
		 AutoMlSettings automlsettings = AutoMlSettings.newBuilder().setCredentialsProvider(credentialsProvider).build();
		 datasetServiceClient = AutoMlClient.create(automlsettings);
		 
		 com.google.cloud.automl.v1beta1.AutoMlSettings tablessettings = com.google.cloud.automl.v1beta1.AutoMlSettings.newBuilder().setCredentialsProvider(credentialsProvider).build();
		 tablesClient = com.google.cloud.automl.v1beta1.AutoMlClient.create(tablessettings);
		 
		 PredictionServiceSettings predectionsettings = PredictionServiceSettings.newBuilder().setCredentialsProvider(credentialsProvider).build();
		 predictionclient = PredictionServiceClient.create(predectionsettings);
		 
		 com.google.cloud.automl.v1beta1.PredictionServiceSettings predectiontablessettings = com.google.cloud.automl.v1beta1.PredictionServiceSettings.newBuilder().setCredentialsProvider(credentialsProvider).build();
		 predictiontableclient = com.google.cloud.automl.v1beta1.PredictionServiceClient.create(predectiontablessettings);
		 

	}
	

	  
	  public String uploadDatasetFiles( String bucketName, String type, String folderPath, String dirPath, String autoMLtype) throws Exception {

		  
		  folderPath = (folderPath == null) ? "" : folderPath;
		  folderPath = (folderPath == "") ? "" : folderPath+"/";
	     
	      List<String> objects = storage.uploadDir(bucketName, folderPath, dirPath);

	      String metadataPath = folderPath+"trainconfig";
	      storage.createFolder(bucketName, metadataPath);
	      
	      String  csvObject = "";
	      
	      if (autoMLtype == TEXTEXTRACTION) {
	    	  String tempDir = System.getProperty("java.io.tmpdir");
	    	  String typename = (type == "") ? "AUTOMATIC" : type;
	    	  String jsonlFilename = typename +"_DATA.jsonl";
	    	  File jsonlfile = new File(tempDir + "/"+ jsonlFilename);
	    	  if (jsonlfile.exists()) {
	    		  jsonlfile.delete();
	    	  }
		  
	    	  FileWriter jsonlwriter = new FileWriter(jsonlfile);
	    	  for (Iterator iterator = objects.iterator(); iterator.hasNext();) {
	    		  String object = (String) iterator.next();
	    		  String jsonL = "{\"document\": {\"input_config\": {\"gcs_source\": {\"input_uris\": [ \""+"gs://"+bucketName+"/"+object+"\"]}}}}\n";
	    		  jsonlwriter.write(jsonL);
	    	  }
			
	    	  jsonlwriter.close();
		 
	    	  String jsonlObject = storage.uploadFile(bucketName, metadataPath,jsonlFilename ,jsonlfile.getAbsolutePath());
	      
		      jsonlfile.delete();
   
	    	  File importcsv = new File(tempDir + "/import.csv");
	    	  if (importcsv.exists()) {
	    		  importcsv.delete();
	    	  }
	    	  FileWriter csvwriter = new FileWriter(importcsv);
	    	  csvwriter.write(type+",gs://"+bucketName+"/"+jsonlObject);
	    	  csvwriter.close();
		      csvObject = storage.uploadFile(bucketName, metadataPath,"import.csv" ,importcsv.getAbsolutePath());
    	      importcsv.delete();
    	      
	    	  
	      }
		  
	      
	      
	      if (autoMLtype == VISIONCLASSIFY) {
	    	  String tempDir = System.getProperty("java.io.tmpdir");
	
	    	  File importcsv = new File(tempDir + "/import.csv");
	    	  FileWriter csvwriter = new FileWriter(importcsv);

	    	  for (Iterator iterator = objects.iterator(); iterator.hasNext();) {
	    		  String object = (String) iterator.next();
	    		  String csvline = "gs://"+bucketName+"/"+object+"\n";
	    		  csvwriter.write(csvline);
	    	  }
	    	  csvwriter.close();
		      csvObject = storage.uploadFile(bucketName, metadataPath,"import.csv" ,importcsv.getAbsolutePath());
    	      importcsv.delete();
    	      
	    	  
	      }
	      
	      
	      if (autoMLtype == VISIONDETECTION) {
	    	  String tempDir = System.getProperty("java.io.tmpdir");
	
	    	  File importcsv = new File(tempDir + "/import.csv");
	    	  FileWriter csvwriter = new FileWriter(importcsv);

	    	  for (Iterator iterator = objects.iterator(); iterator.hasNext();) {
	    		  String object = (String) iterator.next();
	    		  String csvline = "UNASSIGNED,gs://"+bucketName+"/"+object+",,,,,,,,,\n";
	    		  csvwriter.write(csvline);
	    	  }
	    	  csvwriter.close();
		      csvObject = storage.uploadFile(bucketName, metadataPath,"import.csv" ,importcsv.getAbsolutePath());
    	      importcsv.delete();
    	      
	    	  
	      }
	      


	      
	      return "gs://"+bucketName+"/"+csvObject;
	      
	      
	  }
	  
	  

	  public String createTextExtractionDataset( String displayName)
	      throws IOException, ExecutionException, InterruptedException {
	
	      LocationName locationName = LocationName.of(this.projectID, this.location);

	      TextExtractionDatasetMetadata metadata = TextExtractionDatasetMetadata.newBuilder().build();
	      Dataset dataset =
	          Dataset.newBuilder()
	              .setDisplayName(displayName)
	              .setTextExtractionDatasetMetadata(metadata)
	              .build();
	      OperationFuture<Dataset, OperationMetadata> future = datasetServiceClient.createDatasetAsync(locationName, dataset);

	      Dataset createdDataset = future.get();

	      String[] names = createdDataset.getName().split("/");
	      String datasetId = names[names.length - 1];
	      return datasetId;
	  }
	  
	  

	  public String createandimportTablesDataset( String displayName, String path)
	      throws IOException, ExecutionException, InterruptedException {
	
		   com.google.cloud.automl.v1beta1.LocationName projectLocation = com.google.cloud.automl.v1beta1.LocationName.of(this.projectID, this.location);
		   TablesDatasetMetadata metadata = TablesDatasetMetadata.newBuilder().build();
		   com.google.cloud.automl.v1beta1.Dataset dataset =
		    		  com.google.cloud.automl.v1beta1.Dataset.newBuilder()
		              .setDisplayName(displayName)
		              .setTablesDatasetMetadata(metadata)
		              .build();
		  com.google.cloud.automl.v1beta1.Dataset createdDataset = tablesClient.createDataset(projectLocation, dataset);
		  String[] names = createdDataset.getName().split("/");
		  String datasetId = names[names.length - 1];
		  
		  com.google.cloud.automl.v1beta1.DatasetName datasetFullId = com.google.cloud.automl.v1beta1.DatasetName.of(this.projectID, this.location, datasetId);

		  com.google.cloud.automl.v1beta1.InputConfig.Builder inputConfigBuilder = com.google.cloud.automl.v1beta1.InputConfig.newBuilder();

		  com.google.cloud.automl.v1beta1.GcsSource gcsSource =com.google.cloud.automl.v1beta1.GcsSource.newBuilder().addAllInputUris(Arrays.asList(path.split(","))).build();
		  inputConfigBuilder.setGcsSource(gcsSource);

		  Empty response = tablesClient.importDataAsync(datasetFullId, inputConfigBuilder.build()).get();
		  
	      return datasetId;
	  }
	  
	  
	  
	  
	  public String createImageClassificationDataset( String displayName,  String classificationtype)
		      throws IOException, ExecutionException, InterruptedException {

		  	  LocationName projectLocation = LocationName.of(this.projectID,this.location);

		    
		      ClassificationType classificationType = ClassificationType.valueOf(classificationtype);
		      ImageClassificationDatasetMetadata metadata =ImageClassificationDatasetMetadata.newBuilder()
		              .setClassificationType(classificationType)
		              .build();
		      Dataset dataset =
		          Dataset.newBuilder()
		              .setDisplayName(displayName)
		              .setImageClassificationDatasetMetadata(metadata)
		              .build();
		      OperationFuture<Dataset, OperationMetadata> future = datasetServiceClient.createDatasetAsync(projectLocation, dataset);

		      Dataset createdDataset = future.get();
		      String[] names = createdDataset.getName().split("/");
		      String datasetId = names[names.length - 1];
		      return datasetId;
	  
      }
	  
	  
	  public String createImageDetectionDataset( String displayName)
		      throws IOException, ExecutionException, InterruptedException {

		  	  LocationName projectLocation = LocationName.of(this.projectID,this.location);
		  	  
		      ImageObjectDetectionDatasetMetadata metadata =
		              ImageObjectDetectionDatasetMetadata.newBuilder().build();
		      Dataset dataset = Dataset.newBuilder()
		                  		.setDisplayName(displayName)
		                  		.setImageObjectDetectionDatasetMetadata(metadata)
		                  		.build();
	
		      OperationFuture<Dataset, OperationMetadata> future = datasetServiceClient.createDatasetAsync(projectLocation, dataset);

		      Dataset createdDataset = future.get();
		      String[] names = createdDataset.getName().split("/");
		      String datasetId = names[names.length - 1];
		      return datasetId;
	  
      }
		    
		    
	  public String importDataset( String datasetId,  String path)
		      throws IOException, ExecutionException, InterruptedException, TimeoutException {

		  DatasetName datasetFullId = DatasetName.of(this.projectID, this.location, datasetId);

	      // Get multiple Google Cloud Storage URIs to import data from
	      GcsSource gcsSource = GcsSource.newBuilder().addAllInputUris(Arrays.asList(path.split(","))).build();

	      // Import data from the input URI

	      InputConfig inputConfig = InputConfig.newBuilder().setGcsSource(gcsSource).build();

	      // Start the import job
	      OperationFuture<Empty, OperationMetadata> operation =   datasetServiceClient.importDataAsync(datasetFullId, inputConfig);

	      return operation.getName();

	    
	  }

	  
	  public String getOperationStatus(String operationName) {
		  boolean done = datasetServiceClient.getOperationsClient().getOperation(operationName).getDone();
		  String status = "UNKNOWN";
		  if (!done) {
			  status = "IN PROGRESS";
		  }
		  else {
			 if (datasetServiceClient.getOperationsClient().getOperation(operationName).hasError()) {
				 status = "ERROR";
			 }
			 else {
				 status = "DONE";
			 }
			
		  }
		  return status;
	  }
	  
	  
	  
	  public String createTextExtractionModel( String datasetId, String displayName )
		      throws IOException, ExecutionException, InterruptedException {
		    
		      // A resource that represents Google Cloud Platform location.
		      LocationName projectLocation = LocationName.of(this.projectID, this.location);
		      // Set model metadata.
		      TextExtractionModelMetadata metadata = TextExtractionModelMetadata.newBuilder().build();
		      Model model =Model.newBuilder()
		              .setDisplayName(displayName)
		              .setDatasetId(datasetId)
		              .setTextExtractionModelMetadata(metadata)
		              .build();

		      OperationFuture<Model, OperationMetadata> operation = datasetServiceClient.createModelAsync(projectLocation, model);
		     
		      return operation.getName();
		      
	 }
	  
	  
	  
	  public HashMap<String,List<Object>> predict( String modelId, String content, String type) throws IOException {

		      // Get the full path of the model.
		      ModelName name = ModelName.of(this.projectID, this.location, modelId);
		      ExamplePayload payload = null;;
		      HashMap<String,List<Object>> result = new HashMap<String,List<Object>>();
		      switch (type) {
		      	case TEXTEXTRACTION:
		      	case TEXTCLASSIFY:
		      				TextSnippet textSnippet = TextSnippet.newBuilder().setContent((String)content).setMimeType("text/plain").build();
		      				payload = ExamplePayload.newBuilder().setTextSnippet(textSnippet).build();

		      				break;
		      	case VISIONCLASSIFY:	
		      	case VISIONDETECTION:		      				
		      				ByteString contentImage = ByteString.copyFrom(Files.readAllBytes(Paths.get((String)content)));
		      				Image image = Image.newBuilder().setImageBytes(contentImage).build();
		      				payload = ExamplePayload.newBuilder().setImage(image).build();
		      				break;
		      			
		      				
		      }
		      	 
		      
		      				 
		      PredictRequest predictRequest =  PredictRequest.newBuilder().setName(name.toString()).setPayload(payload).build();
		      PredictResponse response = predictionclient.predict(predictRequest);
		      
		      
		      switch (type) {
		      	case TEXTEXTRACTION:
				      		for (AnnotationPayload annotationPayload : response.getPayloadList()) {
				      			JSONObject value = new JSONObject();
				      			value.put("score",(Float)annotationPayload.getTextExtraction().getScore());
				      			value.put("content",annotationPayload.getTextExtraction().getTextSegment().getContent());
				      			value.put("endoffset",(Long)annotationPayload.getTextExtraction().getTextSegment().getEndOffset());
				      			value.put("startoffset",(Long)annotationPayload.getTextExtraction().getTextSegment().getStartOffset());
				      			result.computeIfAbsent(annotationPayload.getDisplayName(), k -> new ArrayList<Object>()).add(value.toString());
				      		}
					        break;
		      	case TEXTCLASSIFY:
		      				for (AnnotationPayload annotationPayload : response.getPayloadList()) {
		      					 result.computeIfAbsent(annotationPayload.getDisplayName(), k -> new ArrayList<Object>()).add((Float)annotationPayload.getClassification().getScore());
		      				}
		      				break;
		    	case VISIONCLASSIFY:	
		    				for (AnnotationPayload annotationPayload : response.getPayloadList()) {
		      					 result.computeIfAbsent(annotationPayload.getDisplayName(), k -> new ArrayList<Object>()).add((Float)annotationPayload.getClassification().getScore());
		    				}	
		 		    		break;
		 		    		
		    	case VISIONDETECTION:	
    						for (AnnotationPayload annotationPayload : response.getPayloadList()) {
    							    JSONObject resultJSON = new JSONObject();
    								resultJSON.put("score",(Float)annotationPayload.getImageObjectDetection().getScore());
    								JSONArray verticesJSON = new JSONArray();
    								BoundingPoly boundingPoly = annotationPayload.getImageObjectDetection().getBoundingBox();
    								for (NormalizedVertex vertex : boundingPoly.getNormalizedVerticesList()) {
                                          JSONObject vertexJSON= new JSONObject();
                                          vertexJSON.put("x", vertex.getX());
                                          vertexJSON.put("y", vertex.getY());
                                          verticesJSON.put(vertexJSON);
    								}
    								resultJSON.put("boundingbox", verticesJSON);
    								result.computeIfAbsent(annotationPayload.getDisplayName(), k -> new ArrayList<Object>()).add(resultJSON.toString());
    						}	
    						break;
		      				
		      }
		      
		      return result;

		  }
	  
	  public HashMap<String,String> getModels() throws IOException {
		  
		  HashMap<String,String>   modelMap = new  HashMap<String,String>();
		  
		  LocationName projectLocation = LocationName.of(this.projectID,this.location);

	      // Create list models request.
	      ListModelsRequest listModelsRequest =  ListModelsRequest.newBuilder()
	              .setParent(projectLocation.toString())
	              .setFilter("")
	              .build();

	      // List all the models available in the region by applying filter.

	      for (Model model : datasetServiceClient.listModels(listModelsRequest).iterateAll()) {
	    	  String[] names = model.getName().split("/");
	    	  String retrievedModelId = names[names.length - 1];
	    	  modelMap.put(retrievedModelId, model.getDisplayName());
	      }  
	      
	      return modelMap;

	  }
	  
	  
	  public HashMap<String,Object> getModelDetails(String modelID) throws IOException {
		  
		  
		  com.google.cloud.automl.v1beta1.ModelName modelFullId = com.google.cloud.automl.v1beta1.ModelName.of(this.projectID, this.location, modelID);
	      com.google.cloud.automl.v1beta1.Model model = tablesClient.getModel(modelFullId);
	      

		  HashMap<String,Object>   modelDetailsMap = new  HashMap<String,Object>();
	      modelDetailsMap.put("displayname", model.getDisplayName());
	      modelDetailsMap.put("name", model.getName());
	      String[] names = model.getName().split("/");
	      String retrievedModelId = names[names.length - 1];
	      modelDetailsMap.put("id", retrievedModelId);
	      modelDetailsMap.put("state", model.getDeploymentState().toString());
	      
	      Map<FieldDescriptor, Object> tablesmetadata = null ;
	      if (model.hasTablesModelMetadata()) {
	    	  tablesmetadata = model.getTablesModelMetadata().getAllFields();

	    	  HashMap<String,String>   modelColumns = new  HashMap<String,String>();
	      
	    	  for (Entry<FieldDescriptor, Object> entry:tablesmetadata.entrySet()) {
	       		  FieldDescriptor fieldDescriptor = entry.getKey();
	    		  if (fieldDescriptor.getName().equals("tables_model_column_info")) {
	    			  List<TablesModelColumnInfo> column_info = (List<com.google.cloud.automl.v1beta1.TablesModelColumnInfo>) entry.getValue();
	    			  for (Iterator iterator = column_info.iterator(); iterator.hasNext();) {
						TablesModelColumnInfo tablesModelColumnInfo = (TablesModelColumnInfo) iterator.next();
		    			  String[] specnames = tablesModelColumnInfo.getColumnSpecName().split("/");
		    			  modelColumns.put(tablesModelColumnInfo.getColumnDisplayName(), specnames[specnames.length-1]);
	    			  }

	    		 }

	    	  }
	    	  
	    	  modelDetailsMap.put("columns",  modelColumns);
	      
	      }

	      return modelDetailsMap;

	  }
	  
	  
	  public HashMap<String,Object> predictTables(String modelId, Map<String,String> values ,Map<String,String> columns  ) throws IOException {
		  
	      ModelName name = ModelName.of(this.projectID, this.location, modelId);
	      HashMap<String,Object> result = new HashMap<String,Object>();

	      Builder rowBuilder = com.google.cloud.automl.v1beta1.Row.newBuilder();
	      
	      for (Entry<String, String> value:values.entrySet()) {
	    	  if (columns.containsKey(value.getKey())) {
	    		  String columnspecID = columns.get(value.getKey());
	    		  String columnValue = value.getValue();
	    		  rowBuilder = rowBuilder.addColumnSpecIds(columnspecID).addValues(Value.newBuilder().setStringValue(columnValue));
	    	  }
			
		}
	      Row row = rowBuilder.build();
		  com.google.cloud.automl.v1beta1.ExamplePayload payload = com.google.cloud.automl.v1beta1.ExamplePayload.newBuilder().setRow(row).build();

		  com.google.cloud.automl.v1beta1.PredictRequest request =
						  com.google.cloud.automl.v1beta1.PredictRequest.newBuilder().setName(name.toString())
						  .setPayload(payload)
						  .putParams("feature_importance", "true")
						  .build();

		  com.google.cloud.automl.v1beta1.PredictResponse response = predictiontableclient.predict(request);
		  for (com.google.cloud.automl.v1beta1.AnnotationPayload annotationPayload : response.getPayloadList()) {
			  com.google.cloud.automl.v1beta1.TablesAnnotation tablesAnnotation = annotationPayload.getTables();
			  result.put(tablesAnnotation.getValue().getStringValue(), (Float)tablesAnnotation.getScore());
		  }
		  
		  return result;
	  }
		  
		
}
