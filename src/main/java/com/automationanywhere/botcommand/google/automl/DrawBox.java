/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */


package com.automationanywhere.botcommand.google.automl;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

import java.util.Optional;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

import com.automationanywhere.commandsdk.annotations.Execute;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Draw and Store Detected Box", name = "drawboundingbox",
        description = "Draws the detected bounding box and save it",
        node_label = "Draw and Store Detected Box", icon = "pkg.svg", background_color = "#EDEDED" , comment = true )

public class DrawBox {
	
	  private static final Logger logger = LogManager.getLogger(DrawBox.class);
	
	   
	@Execute
    public void action(@Idx(index = "1", type = AttributeType.FILE)  @Pkg(label = "Input Image" , default_value_type = DataType.FILE) @NotEmpty String input,
    				   @Idx(index = "2", type = AttributeType.FILE)  @Pkg(label = "Output Image" , default_value_type = DataType.FILE ) @NotEmpty String output,
    				   @Idx(index = "3", type = AttributeType.FILE)  @Pkg(label = "Sub Image" , default_value_type = DataType.FILE ) @NotEmpty String suboutput,
    				   @Idx(index = "4", type = TEXT)  @Pkg(label = "Annotation" , default_value_type = STRING )  String annotation,
    				   @Idx(index = "5", type = TEXT)  @Pkg(label = "Bounding Box (JSON)" , default_value_type = STRING ) @NotEmpty String jsonBox,
    				   @Idx(index = "6", type = AttributeType.RADIO, options = {
  								@Idx.Option(index = "6.1", pkg = @Pkg(label = "Yes", value = "yes")),
  								@Idx.Option(index = "6.2", pkg = @Pkg(label = "No", value = "no"))
     				   			}) @Pkg(label = "Normalized Vertices", default_value = "no", default_value_type = STRING) @NotEmpty String normalized ) throws Exception { 


        JSONArray box = new JSONArray(jsonBox);
        
        annotation = (annotation == null) ? "" : annotation;
        
        BufferedImage in = ImageIO.read(new File(input));
        int w = (normalized == "yes") ? in.getWidth() : 1;
        int h = (normalized == "yes") ? in.getHeight(): 1;

		Graphics2D g = (Graphics2D) in.getGraphics();
		g.setStroke(new BasicStroke(3));
		int[] x;
		int[] y;
        if (box.length() == 2) {
      
        	x = new int[box.length()*2];
        	y = new int[box.length()*2];
        	x[0] = (int)(box.getJSONObject(0).getFloat("x")*w);
        	y[0] = (int)(box.getJSONObject(0).getFloat("y")*h);		
        	x[1] = (int)(box.getJSONObject(0).getFloat("x")*w);
        	y[1] = (int)(box.getJSONObject(1).getFloat("y")*h);	  
        	x[2] = (int)(box.getJSONObject(1).getFloat("x")*w);
        	y[2] = (int)(box.getJSONObject(1).getFloat("y")*h);
        	x[3] = (int)(box.getJSONObject(1).getFloat("x")*w);
        	y[3] = (int)(box.getJSONObject(0).getFloat("y")*h);
        }
        else {
        	x = new int[box.length()];
        	y = new int[box.length()];
        	x[0] = (int)(box.getJSONObject(0).getFloat("x")*w);
        	y[0] = (int)(box.getJSONObject(0).getFloat("y")*h);		
        	x[1] = (int)(box.getJSONObject(1).getFloat("x")*w);
        	y[1] = (int)(box.getJSONObject(1).getFloat("y")*h);	  
        	x[2] = (int)(box.getJSONObject(2).getFloat("x")*w);
        	y[2] = (int)(box.getJSONObject(2).getFloat("y")*h);
        	x[3] = (int)(box.getJSONObject(3).getFloat("x")*w);
        	y[3] = (int)(box.getJSONObject(3).getFloat("y")*h);
        	
        }
		g.setComposite(AlphaComposite.SrcOver.derive(0.6f));
		g.setColor(Color.GREEN);
		g.drawPolygon(x,y,4);

 	     if (annotation != "") {
 			Font stringFont = new Font("Arial",Font.BOLD,10);
 			g.setFont(stringFont);
	 	   g.drawString(annotation, x[1]+2, y[1]+2);
 	     }
		File outputfile = new File(output);
		ImageIO.write(in, getExtensionByStringHandling(input).get(), outputfile);
		
		int subw = Math.abs(x[2]- x[0]);
		int subh = Math.abs(y[2]- y[0]);

		BufferedImage subimage = in.getSubimage(x[0],y[0],subw,subh);
		File suboutputfile = new File(suboutput);
		ImageIO.write(subimage, getExtensionByStringHandling(input).get(), suboutputfile);
	
	}

	
	 public Optional<String> getExtensionByStringHandling(String filename) {
		    return Optional.ofNullable(filename)
		      .filter(f -> f.contains("."))
		      .map(f -> f.substring(filename.lastIndexOf(".") + 1));
		}
   	
	
}