package com.automationanywhere.botcommand.google.automl.utils;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.google.api.gax.paging.Page;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageClass;
import com.google.cloud.storage.StorageException;
import com.google.cloud.storage.StorageOptions;

import java.nio.file.Files;
import java.nio.file.Path;


public class GoogleStorageBucket {
	
	Storage storage;

	public void auth(String jsonPath) throws Exception {
	
		
		 Collection<String> scopes = new ArrayList<String>();
		 scopes.add("https://www.googleapis.com/auth/cloud-platform");
		 GoogleCredentials credentials = GoogleCredentials.fromStream(new FileInputStream(jsonPath)).createScoped(scopes);
	     storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
			  
	}

	  public HashMap<String,String> createBucketWithStorageClassAndLocation(String bucketName, String location, String storageclass) {

	    StorageClass storageClass = StorageClass.valueOf(storageclass);
	    	Bucket bucket =
	        storage.create(
	            BucketInfo.newBuilder(bucketName)
	                .setStorageClass(storageClass)
	                .setLocation(location)
	                .build());
        HashMap<String,String> result = new HashMap<String,String>();
        result.put("bucktname", bucket.getName());
        result.put("location", bucket.getLocation());
        result.put("storageclass", bucket.getStorageClass().name());
        result.put("id", bucket.getGeneratedId());
        
        return result;


	  }
	  
	  public String deleteBucket(String bucketName) {

		    Bucket bucket = storage.get(bucketName);
		    bucket.delete();
		    return "SUCCESS";


     }
	  
	  public  List<String> listBuckets() {
		 
		  Page<Bucket> buckets = storage.list();

		  List<String> result = new ArrayList<String>();
		  for (Bucket bucket : buckets.iterateAll()) {
			 result.add(bucket.getName());
		  }
		    
		  return result;
	  }
	  
	  
	  public  HashMap<String,Object> getBucketDetails(String bucketName) {
			 
		  Bucket bucket = storage.get(bucketName);

		  HashMap<String,Object> result = new HashMap<String,Object>();
		  result.put("name",bucket.getName());
		  result.put("id",bucket.getGeneratedId());
		  result.put("location",bucket.getLocation());
		  result.put("locationtype",bucket.getLocationType());
		  result.put("storageclass",bucket.getStorageClass().name());
		  result.put("created",ZonedDateTime.ofInstant(Instant.ofEpochMilli(bucket.getCreateTime()), ZoneId.systemDefault()));
		  result.put("uri",bucket.getSelfLink());
		  return result;
	  }
	  
	  
	  public String createFolder( String bucketName, String  folderPath) throws Exception {
			

		  	folderPath = folderPath.replace("\\", "/")+"/";
		    BlobId blobId = BlobId.of(bucketName, folderPath);
		    BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();

		    Blob storageobj = storage.create(blobInfo,"".getBytes());

		    return storageobj.getName();
	  }
	  
	  
	  public String uploadFile( String bucketName, String folderPath, String objectName, String filePath) throws Exception {
	
	    	Path path = Paths.get(filePath);
	    	objectName  = path.getFileName().toString();
		    folderPath = (folderPath == null) ? "" : folderPath;
		    
		    if (objectName == null) {
		    	objectName  = path.getFileName().toString();
		    }
		    if (objectName.isEmpty()) {
		    	objectName  = path.getFileName().toString();
		    }
		    
		   
		    if (folderPath != "") {
		    	objectName = folderPath.replace("\\", "/")+"/"+objectName;
		    }
		    BlobId blobId = BlobId.of(bucketName, objectName);
		    BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();

		    Blob storageobj = storage.create(blobInfo, Files.readAllBytes(Paths.get(filePath)));

		    return storageobj.getName();
	  }
	  
	  public List<String> uploadDir (String bucketName,String  folderPath, String dirPath) throws Exception {
		
	        // get the name of the parent directory
		    dirPath = dirPath.replace("\\", "/");
		    folderPath = (folderPath == null) ? "" : folderPath;
		    if (folderPath != "") {
		      folderPath  = (folderPath.charAt(folderPath.length()-1) == '/' ) ? folderPath : folderPath+"/";
		    }
		    folderPath = folderPath.replace("\\", "/");
	        String[] path = dirPath.split("/");
	        String folder = path[path.length - 1];

	        //get files in main directory
	        File[] files = new File(dirPath).listFiles();

	        // define your projectID & bucket name
	        List<String> uploads = new ArrayList<String>();
	        uploadFolder(files, folder, bucketName, folderPath,  storage,uploads);
	    

		    return uploads;
	  }
	  

	  private void uploadFolder(File[] files, String folder, String bucket,String folderPath, Storage storage, List<String> uploads) throws Exception {
		  for (File file : files) {
			  if (!file.isHidden()) {
				  // if it is a directory read the files within the subdirectory
				  if (file.isDirectory()) {
					  
					  String[] lpath = file.getAbsolutePath().replace("\\", "/").split("/");
					  String lfolder = lpath[lpath.length - 1];
					  String xfolder =    folder + "/" + lfolder;
					  uploadFolder(file.listFiles(), xfolder, bucket, folderPath, storage,uploads); // Calls same method again.

				  } else {
					  // add directory/subdirectory to the file name to create the file structure
					  BlobId blobId = BlobId.of(bucket,folderPath + folder + "/" + file.getName());
					
					  //prepare object
					  BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();

					  // upload object
				
					  Blob storageobj = storage.create(blobInfo, Files.readAllBytes(Paths.get(file.getAbsolutePath())));
					  uploads.add(storageobj.getName());

				  }

			  }

		  }
	  } 
	  
	  
	  public List<String> listObjects( String bucketName, String folderPath ) {

		    Page<Blob> blobs = storage.list(bucketName);
		    List<String> result = new ArrayList<String>();
	    	folderPath = (folderPath == null) ? "" : folderPath;
		    folderPath = folderPath.replace("\\", "/");
		    for (Blob blob : blobs.iterateAll()) {
		      String objName = blob.getName();
		      if (objName.startsWith(folderPath)  ) {
		    		 result.add(blob.getName());
		      }
		      
		    }
		    
		    return result;
	  }
	  
	  
	  
	  
	  public HashMap<String,Object> getObjectDetails(String bucketName, String objectName)
		      throws StorageException {


		    Blob blob = storage.get(bucketName, objectName, Storage.BlobGetOption.fields(Storage.BlobField.values()));
		    HashMap<String,Object> result = new HashMap<String,Object>();
		    result.put("bucket", blob.getBucket());
		    result.put("encoding", blob.getContentDisposition());
		    result.put("contenttype", blob.getContentType());
		    result.put("name", blob.getName());
		    result.put("size", blob.getSize());
		    result.put("storageclass", blob.getStorageClass().name());
			result.put("created",ZonedDateTime.ofInstant(Instant.ofEpochMilli(blob.getCreateTime()), ZoneId.systemDefault()));
			result.put("modified",ZonedDateTime.ofInstant(Instant.ofEpochMilli(blob.getUpdateTime()), ZoneId.systemDefault()));
			result.put("uri",String.valueOf(blob.getSelfLink()));
			
			return result;
	  }
	  
	  
	  public String deleteObject( String bucketName, String objectName) {

		    storage.delete(bucketName, objectName);
		    
		    return "SUCCESS";
	  }
		 
	  
	  
	  
	  public String downloadObject( String bucketName, String objectName, String destFilePath) {

		  	Blob blob = storage.get(BlobId.of(bucketName, objectName));
		    blob.downloadTo(Paths.get(destFilePath));
		    
		    return "SUCCESS";

	  }
	  

}
