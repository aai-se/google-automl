/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */


package com.automationanywhere.botcommand.google.automl;

import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.google.automl.utils.GoogleAutoML;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.commandsdk.annotations.Execute;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Predict", name = "automlpredict",
        description = "Predictions for Images and Text",
        node_label = "Predict {{sessionName}}|", icon = "pkg.svg", background_color = "#EDEDED" , comment = true ,
        return_type=DataType.DICTIONARY, return_sub_type = DataType.ANY,  return_label="Dictionary of predictions in the format <key,list<value>> ", return_required=true)

public class PredictTextImages {
	
	  private static final Logger logger = LogManager.getLogger(PredictTextImages.class);
	
    @Sessions
    private Map<String, Object> sessions;
    
	   
	@Execute
    public DictionaryValue  action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "AutoMLSession") @NotEmpty String sessionName,
    						  @Idx(index = "2", type = TEXT)  @Pkg(label = "Model ID" , default_value_type = STRING ) @NotEmpty String modelId,
    						  @Idx(index = "3", type = TEXT)  @Pkg(label = "Content" , default_value_type = STRING , description = "Either text string or image file path") @NotEmpty String content,
    						  @Idx(index = "4", type = SELECT, options = {
    	            					    @Idx.Option(index ="4.1", pkg = @Pkg(label = "TEXTEXTRACTION", value = GoogleAutoML.TEXTEXTRACTION)),
    	            					    @Idx.Option(index ="4.2", pkg = @Pkg(label = "VISIONCLASSIFY", value = GoogleAutoML.VISIONCLASSIFY)),
    	            					    @Idx.Option(index ="4.3", pkg = @Pkg(label = "VISIONDETECTION", value = GoogleAutoML.VISIONDETECTION))
    	            					    }) 
                                 @Pkg(label = "AutoML Type"  , default_value = "TEXTEXTRACTION", default_value_type = DataType.STRING , return_sub_type = DataType.STRING) @NotEmpty String type
    						 ) throws Exception { 
		


	    GoogleAutoML autoML  = (GoogleAutoML) this.sessions.get(sessionName);  
	    	    
	    HashMap<String, List<Object>> result = autoML.predict( modelId,content, type);
	    
	    HashMap<String,Value> resultMap = new HashMap<String,Value>();
	    
	    for (Entry<String, List<Object>> entry : result.entrySet()) {
	    	List<Object> values = entry.getValue();
	    	List<Value> valuelist = new ArrayList<Value>();
	    	for (Iterator iterator = values.iterator(); iterator.hasNext();) {
				Object value =  iterator.next();
				String classname = value.getClass().getSimpleName();

				switch (classname) {
					case "String":	
					valuelist.add(new StringValue((String)value));
					break;
				case "Integer":
					valuelist.add(new NumberValue((Integer)value));
					break;
				case "Long":
					valuelist.add(new NumberValue((Long)value));
					break;
				case "Double":
					valuelist.add(new NumberValue((Double)value));
					break;
				case "Float":
					valuelist.add(new NumberValue((Float)value));
					break;
				case "Boolean":
					valuelist.add(new BooleanValue((Boolean)value));
					break;
				default:
					valuelist.add(new StringValue((String)value));
					break;
				}
			
	    	}
	    	ListValue listvalue = new ListValue<>();
	    	listvalue.set(valuelist);
	    	resultMap.put(entry.getKey(), listvalue);
	    }
	    DictionaryValue resultDict = new DictionaryValue();
	    resultDict.set(resultMap);
	    
	    return resultDict;

	
	}

	
	public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
   	
	
}