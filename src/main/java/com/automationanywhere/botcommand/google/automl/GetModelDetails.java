/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */


package com.automationanywhere.botcommand.google.automl;

import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.google.automl.utils.GoogleAutoML;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.commandsdk.annotations.Execute;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Get Model Details", name = "automlmodeldetails",
        description = "Get Model Details",
        node_label = "Get Model Details {{sessionName}}|", icon = "pkg.svg", background_color = "#EDEDED" , comment = true ,
        return_type=DataType.DICTIONARY, return_sub_type = DataType.ANY,  return_description = "In case of Tables model, key 'columns' contains column spec dictionary" , return_label="Model details dictionary", return_required=true)

public class GetModelDetails {
	
	  private static final Logger logger = LogManager.getLogger(GetModelDetails.class);
	
    @Sessions
    private Map<String, Object> sessions;

	   
	@Execute
    public DictionaryValue  action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "AutoMLSession") @NotEmpty String sessionName,
    							   @Idx(index = "2", type = TEXT)  @Pkg(label = "Model ID" , default_value_type = STRING ) @NotEmpty String modelID
    						 ) throws Exception { 
		


	    GoogleAutoML autoML  = (GoogleAutoML) this.sessions.get(sessionName);  
	    	    
	    HashMap<String,Object> details = autoML.getModelDetails(modelID);
	    HashMap<String,Value> resultMap = new HashMap<String,Value>();
	    HashMap<String,Value> columnMap = new HashMap<String,Value>();
	    
	    for (Entry<String, Object> detail:details.entrySet()) {
	    	if (detail.getKey() != "columns") {
	    		resultMap.put(detail.getKey(),new StringValue(detail.getValue().toString()));
	    	}
	    	else {	    		
	    		HashMap<String,String> columndetails = (HashMap<String, String>) detail.getValue();
	    		for (Entry<String, String> columndetail:columndetails.entrySet()) {
	    			columnMap.put(columndetail.getKey(),new StringValue(columndetail.getValue()));
	    		}
	    		DictionaryValue columnDict = new DictionaryValue();
	    		columnDict.set(columnMap);
	    		resultMap.put("columns",columnDict);
	    	}
	    }
	    
	    DictionaryValue resultDict = new DictionaryValue();
	    resultDict.set(resultMap);
	    
	    return resultDict;

	
	}

	
	public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
   	
	
}