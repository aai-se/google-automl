/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */


package com.automationanywhere.botcommand.google.automl;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.google.automl.utils.GoogleAutoML;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.google.cloud.automl.v1.ClassificationType;
import com.automationanywhere.commandsdk.annotations.Execute;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "New Image Classification Dataset", name = "newimagedataset",
        description = "Create a new dataset for image classification",
        node_label = "New Image Classification Dataset {{sessionName}}|", icon = "pkg.svg", background_color = "#EDEDED" , comment = true ,
        return_type=DataType.STRING,  return_label="Dataset ID", return_required=false)

public class CreateImageClassificationDataset {
	
	  private static final Logger logger = LogManager.getLogger(CreateImageClassificationDataset.class);
	
    @Sessions
    private Map<String, Object> sessions;
    
	
	   
	@Execute
    public StringValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "AutoMLSession") @NotEmpty String sessionName,
    					      @Idx(index = "2", type = TEXT)  @Pkg(label = "Display Name" , default_value_type = STRING ) @NotEmpty String displayName,
    					      @Idx(index = "4", type = AttributeType.SELECT, options = {
    									@Idx.Option(index = "4.1", pkg = @Pkg(label = "Multi Label", value = "MULTILABEL")),
    									@Idx.Option(index = "4.2", pkg = @Pkg(label = "Multi Class", value = "MULTICLASS"))}) 
    			    				    @Pkg(label = "Location", default_value = "MULTILABEL", default_value_type = STRING) @NotEmpty String classificationtype
    					     ) throws Exception { 


	    GoogleAutoML autoML  = (GoogleAutoML) this.sessions.get(sessionName);  
	    
	    String datasetid =  autoML.createImageClassificationDataset( displayName,  classificationtype);

	    return new StringValue(datasetid);

	
	}

	
	public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
   	
	
}