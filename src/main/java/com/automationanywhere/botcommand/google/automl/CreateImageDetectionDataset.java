/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */


package com.automationanywhere.botcommand.google.automl;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.google.automl.utils.GoogleAutoML;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.google.cloud.automl.v1.ClassificationType;
import com.automationanywhere.commandsdk.annotations.Execute;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "New Image Detection Dataset", name = "newimagedetectiondataset",
        description = "Create a new dataset for image object detection",
        node_label = "New Image Detection Dataset {{sessionName}}|", icon = "pkg.svg", background_color = "#EDEDED" , comment = true ,
        return_type=DataType.STRING,  return_label="Dataset ID", return_required=false)

public class CreateImageDetectionDataset {
	
	  private static final Logger logger = LogManager.getLogger(CreateImageDetectionDataset.class);
	
    @Sessions
    private Map<String, Object> sessions;
    
	
	   
	@Execute
    public StringValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "AutoMLSession") @NotEmpty String sessionName,
    					      @Idx(index = "2", type = TEXT)  @Pkg(label = "Display Name" , default_value_type = STRING ) @NotEmpty String displayName
    					     ) throws Exception { 


	    GoogleAutoML autoML  = (GoogleAutoML) this.sessions.get(sessionName);  
	    
	    String datasetid =  autoML.createImageDetectionDataset( displayName);

	    return new StringValue(datasetid);

	
	}

	
	public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
   	
	
}