/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */


package com.automationanywhere.botcommand.google.automl;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.google.automl.utils.GoogleAutoML;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.commandsdk.annotations.Execute;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "New Tables Dataset", name = "newtablesdataset",
        description = "Create a new dataset for tables predicitions",
        node_label = "New Tables Dataset {{sessionName}}|", icon = "pkg.svg", background_color = "#EDEDED" , comment = true ,
        return_type=DataType.STRING,  return_label="Dataset ID", return_required=false)

public class CreateTablesDataset {
	
	  private static final Logger logger = LogManager.getLogger(CreateTablesDataset.class);
	
    @Sessions
    private Map<String, Object> sessions;

	   
	@Execute
    public StringValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "AutoMLSession") @NotEmpty String sessionName,
    					      @Idx(index = "2", type = TEXT)  @Pkg(label = "Display Name" , default_value_type = STRING ) @NotEmpty String displayName,
    					      @Idx(index = "3", type = TEXT)  @Pkg(label = "CSV File" , description = "Google Storage URI of data file for training",  default_value_type = STRING ) @NotEmpty String path
    					     ) throws Exception { 


	    GoogleAutoML autoML  = (GoogleAutoML) this.sessions.get(sessionName);  
	    
	    String datasetid = autoML.createandimportTablesDataset( displayName, path);

	    return new StringValue(datasetid);

	
	}

	
	public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
   	
	
}